# Jogo_Memoria
Criando  um jogo de memoria  com HTML, CSS e Javascript 
Digital Innovation One 

Neste projeto, vamos construir juntos um jogo da velha, utilizando somente HTML para estruturar nosso site, CSS para estilizar e dar vida a ele, e Javascript para criarmos toda suas interações, desde a escolha do jogador, efetivar uma jogada e dar um vencedor ao jogo. Vamos abordar conceitos básicos sobre as linguagens e como podemos criar projetos bem simples sem muito trabalho, apenas utilizando suas funções básicas e uma pouco de lógica! 

![img4](https://user-images.githubusercontent.com/66983974/118309820-2e66a280-b4c4-11eb-9b2b-43e5dbe41096.jpg)
 ![img3](https://user-images.githubusercontent.com/66983974/118309224-56093b00-b4c3-11eb-99ff-bec8a5ebbf59.jpg)
![img2](https://user-images.githubusercontent.com/66983974/118309280-68837480-b4c3-11eb-945e-c2a18a5c6bfe.jpg)
![img1](https://user-images.githubusercontent.com/66983974/118309299-73d6a000-b4c3-11eb-9e84-17c5e7d9928f.jpg)
